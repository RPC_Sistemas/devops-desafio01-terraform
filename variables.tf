variable "environment" {
  type        = string
  description = ""
}

variable "aws_region" {
  type        = string
  description = ""
}

variable "autoscale_min" {
  default     = "1"
  description = "Minimum autoscale"
}

variable "autoscale_max" {
  default     = "1"
  description = "Maximum autoscale"
}

variable "subnet_counts" {
  default     = 2
  description = "number of subnets"
}

variable "amis" {
  description = "Images avaiables."
  default = {
    # Ubuntu 20.04
    # us-east-1 = "ami-0aa2b7722dc1b5612"
    # us-east-2 = ""
    # us-west-1 = ""
    # us-west-2 = ""

    # Ubuntu 22.04
    us-east-1 = "ami-007855ac798b5175e"
    us-east-2 = "ami-0a695f0d95cefc163"
    us-west-1 = "ami-014d05e6b24240371"
    us-west-2 = "ami-0fcf52bcf5db7b003"
  }
}

variable "sns_topic_arn" {
  default = ""
}

variable "sns-email" {
  default = "rpc.devops@gmail.com"
}