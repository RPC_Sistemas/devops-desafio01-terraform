resource "aws_iam_user" "devops-user" {
  name = "devops-user"
  path = "/"

  tags = {
    Project = local.project
  }
}

resource "aws_iam_access_key" "devops-accesskey-user" {
  user = aws_iam_user.devops-user.name
}

resource "aws_iam_policy" "devops-sns-policy" {
  name        = "devops-sns-policy"
  path        = "/"
  description = "devops-sns-policy"

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = [
          "SNS:Publish"
        ],
        Effect = "Allow",
        Resource = [
          "arn:aws:sns:${var.aws_region}:${local.account_id}:devops-sns"
        ]
      }
    ]
  })
}

resource "aws_iam_user_policy_attachment" "devops-sns-policy-attachment" {
  user       = aws_iam_user.devops-user.name
  policy_arn = aws_iam_policy.devops-sns-policy.arn
}