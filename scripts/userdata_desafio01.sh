#!/bin/bash

# Update and upgrade system
apt-get update && apt-get -y upgrade

# Install Java
# apt-get install -y openjdk-11-jdk
apt install openjdk-17-jdk -y

# Install Jenkins
#wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | apt-key add -
#sh -c 'echo deb https://pkg.jenkins.io/debian-stable binary/ > \
#    /etc/apt/sources.list.d/jenkins.list'

#apt-get update
#apt-get install -y jenkins

# Add Jenkins on startup
#systemctl enable jenkins

#Install Jenkins

curl -fsSL https://pkg.jenkins.io/debian-stable/jenkins.io-2023.key | sudo tee \
  /usr/share/keyrings/jenkins-keyring.asc > /dev/null

echo deb [signed-by=/usr/share/keyrings/jenkins-keyring.asc] \
  https://pkg.jenkins.io/debian-stable binary/ | sudo tee \
  /etc/apt/sources.list.d/jenkins.list > /dev/null

sudo apt-get update

sudo apt-get install -y jenkins
#

# install docker

# apt-get update

# apt-get install -y ca-certificates curl gnupg

# install -m 0755 -d /etc/apt/keyrings

# curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg

# chmod a+r /etc/apt/keyrings/docker.gpg

# echo \
#   "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
#   "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
#   sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

# apt-get update

# apt-get install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

# Add Jenkins on startup
# systemctl enable jenkins

# install docker

curl -fsSl https://get.docker.com | sh

usermod -aG docker jenkins

systemctl restart jenkins
systemctl status jenkins

# install git

apt-get install -y git

sleep 15

git clone https://gitlab.com/RPC_Sistemas/devops-desafio01.git