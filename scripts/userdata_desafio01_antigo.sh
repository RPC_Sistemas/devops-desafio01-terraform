#!/bin/bash
sudo yum update -y
sudo amazon-linux-extras install epel docker java-openjdk11 -y
sudo yum install daemonize -y
sudo wget -O /etc/yum.repos.d/jenkins.repo \
    https://pkg.jenkins.io/redhat-stable/jenkins.repo
sudo rpm --import https://pkg.jenkins.io/redhat-stable/jenkins.io.key
sudo yum upgrade
sudo yum install git jenkins -y

sudo systemctl start docker                  # Inicia o docker
sudo usermod -aG docker jenkins              # Da permissões ao Jenkins para rodar comandos Docker

sudo systemctl daemon-reload
sudo systemctl start jenkins

sleep 15

git clone https://gitlab.com/RPC_Sistemas/devops-desafio01.git