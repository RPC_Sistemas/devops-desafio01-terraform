resource "aws_key_pair" "devops_key" {
  key_name   = "devops-keypair"
  public_key = tls_private_key.rsa.public_key_openssh
}

resource "tls_private_key" "rsa" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "local_file" "devops_key" {
  content  = tls_private_key.rsa.private_key_pem
  filename = "devops-keypair.pem"
}