resource "aws_ec2_managed_prefix_list" "devops-prefix_list" {
  name           = "Devops IP´s Autorizados"
  address_family = "IPv4"
  max_entries    = 5

  dynamic "entry" {
    for_each = local.devops-ip-list
    content {
      cidr        = entry.value.ip
      description = entry.value.description
    }
  }

  tags = {
    Name        = "devops-prefix-list"
    Project     = local.project
    Environment = var.environment
  }
}