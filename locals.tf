data "aws_caller_identity" "current" {}
locals {
  account_id       = data.aws_caller_identity.current.account_id
  project          = "Desafio01"
  aws_profile      = "devops"
  instance_type    = "t2.micro"
  db_instance_type = "db.t2.micro"
  devops-ip-list = [
    {
      ip          = "187.183.41.49/32"
      description = "Casa"
    },
    {
      ip          = "179.125.170.200/32"
      description = "Trabalho"
    }
  ]
}