output "private-subnet-ids" {
  description = "List ID from private subnets created"
  value       = module.vpc.private-subnet-ids
}
output "public-subnet-ids" {
  description = "List ID from public subnets created"
  value       = module.vpc.public-subnet-ids
}

output "ec2_dns" {
  value = module.desafio01.ec2_dns
}

output "sns_topic_arn" {
  value = aws_sns_topic.devops-sns.arn
}

output "avaiable_zones_name" {
  value = module.vpc.avaiable_zones_name
}

output "devops-user" {
  value = aws_iam_user.devops-user.name
}
output "desafio-01-accesskey" {
  sensitive = true
  value     = aws_iam_access_key.devops-accesskey-user.id
}

output "desafio-01-secretkey" {
  sensitive = true
  value     = aws_iam_access_key.devops-accesskey-user.secret
}