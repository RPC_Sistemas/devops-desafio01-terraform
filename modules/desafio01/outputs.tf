output "aws_security_group_sg-ec2_id" {
  value = aws_security_group.sg-ec2.id
}

output "ec2_dns" {
  value = aws_instance.ec2-desafio-01.public_dns
}