resource "aws_security_group" "sg-ec2" {
  name        = "${var.environment}-sg-ec2-devops"
  description = "Habilita entrada ao sg-lb"
  vpc_id      = var.aws_vpc_vpc_id

  ingress {
    description = "Porta alternativa HTTP"
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "SSH para IP especifico"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    prefix_list_ids = [var.prefix-list-id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name        = "sg-ec2-devops"
    Project     = var.project
    Environment = var.environment
  }
}