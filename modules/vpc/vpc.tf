resource "aws_vpc" "vpc" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = true

  tags = {
    Name        = "vpc-desafio-01"
    Project     = var.project
    Environment = var.environment
  }
}