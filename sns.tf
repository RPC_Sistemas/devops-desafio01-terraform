# Criando Tópico SNS
resource "aws_sns_topic" "devops-sns" {
  name = "devops-sns"
}

resource "aws_sns_topic_subscription" "devops-sns-topic" {
  topic_arn = aws_sns_topic.devops-sns.arn
  protocol  = "email"
  endpoint  = var.sns-email
}